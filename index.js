var express = require('express');
var app = module.exports = express();

var expressLess = require('express-less');

app.use('/assets/css', expressLess(__dirname + '/src/assets/less', { compress: true }));
app.use('/assets/css', express.static(__dirname + '/src/assets/css'));
app.use('/assets/img', express.static(__dirname + '/src/assets/img'));
app.use('/assets/fonts', express.static(__dirname + '/src/assets/fonts'));
app.use('/assets/js', express.static(__dirname + '/src/assets/js'));

var logger = require('./logger');
app.use(logger);

var routes = require('./routes');
app.use(routes);

app.listen(3000, function () {
  console.log('Llistening on port 3000!');
});