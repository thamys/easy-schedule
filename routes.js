var express = require('express');
var app = module.exports = express();

var pug = require('pug');

app.set('views', './src/views')
app.set('view engine', 'pug')

app.get('/', function(req, res) {
    res.render('index', { title: 'Dashboard'})
});

var timesheets = require('./routes/timesheet');
app.use('/gerar-horarios', timesheets);

var subjects = require('./routes/subject');
app.use('/disciplinas', subjects);

app.get('/series', function(req, res) {
    res.render('series', { title: 'Séries'})
});

app.get('/turnos', function(req, res) {
    res.render('turnos', { title: 'Turnos'})
});

app.get('/turmas', function(req, res) {
    res.render('turmas', { title: 'Turmas'})
});

app.get('/professores', function(req, res) {
    res.render('professores', { title: 'Professores'})
});

app.get('/horarios', function(req, res) {
    res.render('horarios', { title: 'Horários'})
});