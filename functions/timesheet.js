/**
 * @author Thamyres Guedes
 * @description Arquivo com todas as funções para montar a grade de horarios
 */

var jsonQuery = require('json-query');
var database = require('../data');

var functions = {

    /**
     * @description Função que chama todas as funções necessárias para montar a grade de horarios.
    */
    generate_timesheet: function (){

    // Escolho a turma randomicamente baseado nas turmas que ainda não tem o horário completo
    let picked_class = this.choose_class(database.get_classes())

    // Vejo os horarios disponiveis naquela turma

    // Pego o primeiro horario disponivel

    /* Ver quais disciplinas eu posso alocar naquele horario, 
        - baseado na quantidade de aulas restantes por distribuir daquela disciplina 
        - se ela pode estar naquele horario
            - baseado nas aulas anteriores daquele dia
            - se ele pode se repetir no dia
            - se ele pode se repetir no horario seguinte
    */

    /* Pego os professores que
        - Podem dar aula naquela serie
        - podem dar aula para uma ou mais das disciplinas possiveis
        - tem disponibilidade real naquele horario   
        Trago eles por ordem de prioridade de tipo de contrato e tempo de casa     
    */

    /* Alocar professor naquele horario
        - varro na ordem de prioridade os professores e vejo qual tem preferencia por esse horario, aloco o primeiro encontrado
        - se ninguém tiver preferencia por esse horario, vejo quem não marcou como "não quero" e aloco o primeiro encontrado
        - se ninguém quiser o horario, o ultimo da lista de prioridades é alocado.
    */
    },

    /**
     * @param timetable Horario a ser verificado
     * @param teacher Professor a se verificado
     * @description Calcula a disponibilidade real, que é a disponibilidade marcada + se ele não está alocado em outra turma naquele horario
     */

    /**
     * @description Trago randomicamente uma das turmas que não stão com a grade completa
    */
    choose_class: function(data){
        // Pego todas as turmas que não tem o horário completo
        let result = jsonQuery('classes[*filled_matrix=false]', {
            data: data
        });
        // Gero o índice randomicamente
        let randomIndex = Math.floor(Math.random()* result.value.length);

        // Retorno somente uma
        return result.value[randomIndex];
    },

    /**
     * @description Verificar horários disponíveis da turma
     */
    verify_free_slots: function(picked_class){

    }

    
}
  module.exports = functions;