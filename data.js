/**
 * @author Thamyres Guedes
 * @description Arquivo com as chamadas para todos os dados
 */

var database = {
    get_subjects: function(){
        return require('./data/subject')
    },
    get_shifts: function(){
        return require('./data/shifts')
    },
    get_series: function(){
        return require('./data/series')
    },
    get_classes: function(){
        return require('./data/classes')
    }
}

module.exports = database;