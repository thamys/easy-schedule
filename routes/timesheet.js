var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({ extended: false});

var database = require('../data');
var functions = require('../functions/timesheet');

router.route('/')
    .get(function(request, response){
		response.render('gerar-horarios', { 
            class_picked: functions.choose_class(database.get_classes())
        })
	});

module.exports = router;