FROM node:6-alpine
ENV NODE_ENV development
WORKDIR /usr/src/app
COPY ["package.json", "npm-shrinkwrap.json*", "./"]
RUN npm install --silent && mv node_modules ../
EXPOSE 3000