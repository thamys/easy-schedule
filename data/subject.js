/**
 * @author Thamyres Guedes
 * @description Disciplinas
 */
var subjects = {
    portugues:{
        name: "Portugueês",
        area: "Humanas",
        lvl_complexidade: 1,
    },
    literatura:{
        name: "Literatura",
        area: "Humanas",
        lvl_complexidade: 1,
    },
    matematica:{
        name: "Matemática",
        area: "Exatas",
        lvl_complexidade: 3,
    },
    quimica:{
        name: "Qúmica",
        area: "Exatas",
        lvl_complexidade: 3,
    },
    fisica:{
        name: "Física",
        area: "Exatas",
        lvl_complexidade: 3,
    },
    biologia:{
        name: "Biologia",
        area: "Biológicas",
        lvl_complexidade: 2,
    },
    historia:{
        name: "História",
        area: "Humanas",
        lvl_complexidade: 1,
    },
    geografia:{
        name: "Geografia",
        area: "Humanas",
        lvl_complexidade: 1,
    },
    filosofia:{
        name: "Filosofia",
        area: "Humanas",
        lvl_complexidade: 1,
    },
    ed_fisica:{
        name: "Educação Física",
        area: "Educação Física",
        lvl_complexidade: 1,
    }
}

module.exports = subjects;