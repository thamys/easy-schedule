/**
 * @author Thamyres Guedes
 * @description Professores
 */

var teachers = {
    "maria_helena": {
        nome: "Maria Helena",
        prioridade: 0,
        contrato: {
            concursada: "11/08/2001"
        },
        disciplinas: {
            portugues:{
                series:{
                    "1EF": true,
                    "2EF": true,
                    "3EF": true,
                    "4EF": true,
                    "5EF": true,
                    "6EF": true,
                    "7EF": true,
                    "8EF": true,
                    "9EF": true,
                    "1EM": true,
                    "2EM": true,
                    "3EM": true
                }
            },
            literatura:{
                series:{
                    "1EF": true,
                    "2EF": true,
                    "3EF": true,
                    "4EF": true,
                    "5EF": true,
                    "6EF": true,
                    "7EF": true,
                    "8EF": true,
                    "9EF": true,
                    "1EM": true,
                    "2EM": true,
                    "3EM": true
                }
            },
        },
        horarios:{
            preferenciais:{
                manha:{
                    sexta:{
                        primeiro: true,
                        segundo: true,
                        terceiro: true,
                    }
                }
            },
            indesejados:{
                manha:{
                    segunda: true
                }
            },
            nao_disponiveis:{
                noite: true,
                tarde: true
            }
        }
    }
}