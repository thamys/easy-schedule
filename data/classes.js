/**
 * @author Thamyres Guedes
 * @description Turmas
 */

var classes = {
    "classes":  [
        {
            name: "1A EF",
            serie: "1EF",
            filled_matrix: false
        },
        {
            name: "2A EF",
            serie: "2EF",
            filled_matrix: true
        },
        {
            name: "3A EF",
            serie: "3EF",
            filled_matrix: false
        }
    ]
}


// Depende da turma
// Nenhuma matéria pode ter duas aulas seguidas, só portugues ematematica, mas somente um dia na semana.

module.exports = classes;