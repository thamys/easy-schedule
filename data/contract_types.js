/**
 * @author Thamyres Guedes
 * @description Tipos de contrato
 */

var contract_types = {
    contratado: 2,
    concursado: 1,
    substituto: 3
 }