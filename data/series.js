/**
 * @author Thamyres Guedes
 * @description Séries / Anos escolares brasileiros
 */

 var series = {
    "1EF": {
        nome: "1º ano",
        ensino: "Fundamental",
        ordem: 1,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "2EF": {
       nome: "2º ano",
       ensino: "Fundamental",
       ordem: 2,
       disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "3EF": {
        nome: "3º ano",
        ensino: "Fundamental",
        ordem: 3,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "4EF": {
        nome: "4º ano",
        ensino: "Fundamental",
        ordem: 4,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "5EF": {
        nome: "5º ano",
        ensino: "Fundamental",
        ordem: 5,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "6EF": {
        nome: "6º ano",
        ensino: "Fundamental",
        ordem: 6,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "7EF": {
        nome: "7º ano",
        ensino: "Fundamental",
        ordem: 7,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "8EF": {
        nome: "8º ano",
        ensino: "Fundamental",
        ordem: 8,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "9EF": {
        nome: "9º ano",
        ensino: "Fundamental",
        ordem: 9,
        disciplinas:{
            portugues: 5,
            matematica: 5,
            ciencias: 3,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            ensino_religioso: 1,
            artes: 1
        }
    },
    "1EM": {
        nome: "1º ano",
        ensino: "Médio",
        ordem: 10,
        disciplinas:{
            portugues: 4,
            matematica: 4,
            quimica: 2,
            fisica: 2,
            biologia: 2,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            filosofia: 1
        }
    },
    "2EM": {
        nome: "2º ano",
        ensino: "Médio",
        ordem: 12,
        disciplinas:{
            portugues: 4,
            matematica: 4,
            quimica: 2,
            fisica: 2,
            biologia: 2,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            filosofia: 1
        }
    },
    "3EM": {
        nome: "3º ano",
        ensino: "Médio",
        ordem: 13,
        disciplinas:{
            portugues: 4,
            matematica: 4,
            quimica: 2,
            fisica: 2,
            biologia: 2,
            historia: 3,
            geografia: 3,
            educacao_fisica: 2,
            ingles: 2,
            filosofia: 1
        }
    }
 }